#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
"""
    Se crea esto para facilitar y guiarse en las "columnas" del top50.csv
"""
(Position,
    TrackName,
    ArtistName,
    Genre,
    BeatsPerMinute,
    Energy,
    Danceability,
    Loudness_dB,
    Liveness,
    Valence,
    Lenght,
    Acousticness,
    Speechiness,
    Popularity) = range(14)


def OpenScv():
    """
        Se abre el archivo con el top50 de las canciones
    """
    Todo = []
    with open("top50.csv") as f:
        for Data in f:
            Todo.append(Data.split(","))
    return Todo


def NombreArtista(Artist, cont, x):
    """
        Para no repetir los artistas se hara un array con todo estos
        pero con la condicion de que si no se repiten, si esto se repiten
        se devolvera un None
    """
    comp = True
    if x != 0:
        tam = len(Artist)
        if len(cont) == 14:
            for x in range(0, tam):
                if Artist[x] == cont[ArtistName]:
                    comp = False
        else:
            for x in range(0, tam):
                if Artist[x] == cont[ArtistName+1]:
                    comp = False
    if comp is True:
        if len(cont) == 14:
            return cont[ArtistName]
        else:
            return cont[ArtistName+1]


def Mediana(cont, Med):
    """
        Para conseguir la mediana se hara un listado con todo los ruidos en INT
        Al final de este proceso se hara un sort y se obtendra el Med[25] que
        tendra la mediana
    """
    if len(cont) == 14:
        Med.append(int(cont[Loudness_dB]))
    else:
        Med.append(int(cont[Loudness_dB+1]))
    return Med


def SongIgualMed(Med, cont, Songs):
    """
        Para ver que canciones tienen el mismo valor que la mediana en ruido
        se pasara por el listado, comparandolo con el string de la mediana
        ya que en los datos, todo es String
    """
    if len(cont) == 14:
        if cont[Loudness_dB] == str(Med[25]):
            print("Posicion:", cont[Position])
            print("Artista:", cont[ArtistName])
            print("Nombre:", cont[TrackName])
            print("Genero:", cont[Genre])
            print("\n")
    else:
        if cont[Loudness_dB+1] == str(Med[25]):
            print("Posicion:", cont[Position])
            print("Artista:", cont[ArtistName+1])
            print("Nombre:", cont[TrackName]+cont[TrackName+1])
            print("Genero:", cont[Genre+1])
            print("\n")


def Popularidad_Silenciosa(Datos, Med):
    """
        Para saber cual es la mas popular se hara el cont[Popularity] un int
        el cual ira actualizando pop si y solo si es mayor que este, dejando
        el de mayor popularidad de los primeros

        Al terminar el proceso de saber quien es mas popular, se vera
        si es menosruidosa por medio de buscar lade mas popularidad y
        comparar con Med[0](La menor posible), en el caso que sea verdad
        dira la informacion pero si no es la menos ruidosa, el programa
        respondera con un "NO"
    """
    pop = 0
    comp = False
    for x, cont in enumerate(Datos):
        if x != 0:
            popcan = int(cont[Popularity].replace('"', ''))
            if len(cont) == 14:
                if pop < popcan:
                    pop = popcan
            else:
                if pop < popcan:
                    pop = popcan
    for x, cont in enumerate(Datos):
        if x != 0:
            Numpop = int(cont[Popularity].replace('"', ''))
            if len(cont) == 14:
                if Numpop == pop and cont[Loudness_dB] == str(Med[0]):
                    print("La cancion mas popular y menos ruidosa es:")
                    print("Artista:", cont[ArtistName])
                    print("Cancion: ", cont[TrackName])
                    print("Popularidad:", cont[Popularity].replace('"', ''))
                    print("Ruido:", cont[Loudness_dB])
                    comp = True
            else:
                if Numpop == pop and cont[Loudness_dB+1] == str(Med[0]):
                    print("La cancion mas popular y menos ruidosa es:")
                    print("Artista:", cont[ArtistName+1])
                    print("Cancion: ", cont[TrackName]+cont[TrackName+1])
                    print("Popularidad:", cont[Popularity+1].replace('"', ''))
                    print("Ruido:", cont[Loudness_dB+1])
                    comp = True
    if comp is not True:
        print("NO")


def Bailables(cont, Baile):
    """
        Aca se obtendra un array de que tan bailable es la cancion, el cual
        sera ordenaro para saber cual es el mayor y cual es el menor
    """
    if len(cont) == 14:
        Baile.append(int(cont[Danceability]))
    else:
        Baile.append(int(cont[Danceability+1]))
    return Baile


def Lentas(cont, Ritmo):
    """
        Aca se obtendran un array de los BPM, el cual despues sera ordenado
        para saber cual es el mayor y cual es el menor
    """
    if len(cont) == 14:
        Ritmo.append(int(cont[BeatsPerMinute]))
    else:
        Ritmo.append(int(cont[BeatsPerMinute+1]))
    return Ritmo


def LaMasLentas(Ritmo, cont, limRitmo):
    """
        Aca se obtendra las canciones mas lentas con su genero
        solo se obtendras 3 por medio de obtener una, se suma el limite y
        se devuelve, hasta que sea un total de 3
    """
    comp = False
    if len(cont) == 14:
        #De menor a mayor
        if int(cont[BeatsPerMinute]) == Ritmo[limRitmo]:
            print("La cancion", cont[TrackName], "Es lenta")
            print("Su genero es:", cont[Genre])
            comp = True
    else:
        if int(cont[BeatsPerMinute+1]) == Ritmo[limRitmo]:
            print("La cancion", cont[TrackName+1], "Es lenta")
            print("Su genero es:", cont[Genre+1])
            comp = True
    if comp is True:
        limRitmo = limRitmo + 1
    return limRitmo


def LaMasBailables(Baile, cont, limBaile):
    """
        Aca se obtendra las canciones mas bailables con su genero
        solo se obtendras 3 por medio de obtener una, se suma el limite y
        se devuelve, hasta que sea un total de 3
    """
    comp = False
    if len(cont) == 14:
        # De mayor a menor
        if int(cont[Danceability]) == Baile[49-limBaile]:
            print("La cancion", cont[TrackName], "Es muy bailable")
            print("Su genero es:", cont[Genre])
            comp = True
    else:
        if int(cont[Danceability+1]) == Baile[49-limBaile]:
            print("La cancion", cont[TrackName+1], "Es muy bailable")
            print("Su genero es:", cont[Genre+1])
            comp = True
    if comp is True:
        limBaile = limBaile + 1
    return limBaile


def Json(Datos):
    """
        Para transformarlo en JSON hay que tener en cuenta lo que se quiere
        para obtener lo que se quiere se usara el rango de todas las cosas
        anteriormente establecidas, se pasara por los Datos y con esto se
        creara el JSON, siendo todo los datos guardados en "Data".
        AL terminar el proceso de obtener todo los datos, estos seran guardados
        en un JSON con el comando .dump, ademas, para mantener el order
        se agregara indent = 4, lo cual añadira 4 espacios
    """
    data = {}
    data['Top'] = []
    for x, cont in enumerate(Datos):
        if x != 0:
            if len(cont) == 14:
                data['Top'].append({
                    'Ranking: ': cont[Position],
                    'Cancion: ': cont[TrackName],
                    'Artista: ': cont[ArtistName],
                    'Genero: ': cont[Genre],
                    'Beat: ': cont[BeatsPerMinute],
                    'Lenght: ': cont[Lenght],
                    'Popularity: ': cont[Popularity]})
            else:
                data['Top'].append({
                    'Ranking: ': cont[Position],
                    'Cancion: ': cont[TrackName]+cont[TrackName+1],
                    'Artista: ': cont[ArtistName+1],
                    'Genero: ': cont[Genre+1],
                    'Beat: ': cont[BeatsPerMinute+1],
                    'Lenght: ': cont[Lenght+1],
                    'Popularity: ': cont[Popularity+1]})
    with open('Top50.json', 'w') as f:
        json.dump(data, f, indent=4)


def EntrarLista(Datos):
    """
        Aca es donde se realizaran todo los procesos para conseguir
        las metas de la actividad
    """
    total = 0
    Artist = []
    Pop = []
    Med = []
    Songs = []
    Baile = []
    Ritmo = []
    for x, cont in enumerate(Datos):
        if x == 0:
            pass
        else:
            Singular = NombreArtista(Artist, cont, x)
            Artist.append(Singular)
            Med = Mediana(cont, Med)
    tam = len(Artist)
    print("Los artistas en el top 50 son los siguientes: ")
    for x in range(0, tam):
        # En el caso que el artista sea "None", no se imprimira
        if Artist[x] is not None:
            total = total + 1
            print(Artist[x])
    print("En total son", total, "Artistas")
    print("\n")
    Med.sort()
    # Se colocara 24, ya que comienza del 0 y termina en 49, siendo la mitad
    print("La Mediana del ruido es: ", Med[24])
    print("\n")
    print("Las siguientes canciones son -6 en ruido")
    for x, cont in enumerate(Datos):
        if x != 0:
            SongIgualMed(Med, cont, Songs)
            # Se crearan matrices para el punto c
            Baile = Bailables(cont, Baile)
            Ritmo = Lentas(cont, Ritmo)
    Baile.sort()
    Ritmo.sort()
    # Comprobadores de si se encontraron 3
    limBaile = 0
    limRitmo = 0
    while limBaile != 3 and limRitmo != 3:
        for x, cont in enumerate(Datos):
            if x != 0:
                if limRitmo <= 2:
                    limRitmo = LaMasLentas(Ritmo, cont, limRitmo)
                if limBaile <= 2:
                    limBaile = LaMasBailables(Baile, cont, limBaile)
    print("\n")
    Popularidad_Silenciosa(Datos, Med)
    Json(Datos)


if __name__ == "__main__":
    Datos = OpenScv()
    EntrarLista(Datos)
